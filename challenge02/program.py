#!/usr/bin/env python3 

import sys 
import collections 

#functions for doing arithmatic 
expressions = {
  '+': (lambda a, b: a + b), 
  '-': (lambda a, b: a - b), 
  '*': (lambda a, b: a * b), 
  '/': (lambda a, b: a / b),
  '^': (lambda a, b: a ** b)
}

Node = collections.namedtuple('Node', 'value next')

class Stack(object):
  ''' Implementation of the stack data structure '''

  def __init__(self): 
    self.head = None

  def push(self, value): 
    self.head = Node(value, self.head)

  def pop(self): 
    node = self.head
    self.head = self.head.next
    return node.value

  def empty(self):
    return self.head is None 

  def __str__(self): 
    return 'Stack({})'.format(self.head) 

# uses a stack to compute the equation 
def compute(item): 
  s = Stack()

  for x in item: 
    if x in expressions:  
      v1 = s.pop()
      v2 = s.pop() 
      s.push(expressions[x](int(v2),int(v1)))
    else:
      s.push(x)

  return(s.pop())

def fancyPrint(number):
  
  # for the top line 
  for i in str(number):
    if i == '-':
      top(i)
    if i == '0':
      top(i) 
    if i == '1':
      top(i) 
    if i == '2':
      top(i) 
    if i == '3':
      top(i) 
    if i == '4':
      top(i) 
    if i == '5':
      top(i) 
    if i == '6':
      top(i) 
    if i == '7':
      top(i) 
    if i == '8':
      top(i) 
    if i == '9':
      top(i) 
  print()

  # for the middle line 
  for i in str(number):
   if i == '-':
     middle(i) 
   if i == '0': 
     middle(i)
   if i == '1':
     middle(i) 
   if i == '2':
     middle(i) 
   if i == '3':
     middle(i) 
   if i == '4':
     middle(i) 
   if i == '5':
     middle(i) 
   if i == '6':
     middle(i) 
   if i == '7':
     middle(i) 
   if i == '8':
     middle(i) 
   if i == '9':  
     middle(i) 
  print()

  # for the bottom line 
  for i in str(number):
   if i == '-':
     bottom(i)
   if i == '0': 
     bottom(i)
   if i == '1':
     bottom(i)
   if i == '2':
     bottom(i)
   if i == '3':
     bottom(i)
   if i == '4':
     bottom(i)
   if i == '5':
     bottom(i)
   if i == '6':
     bottom(i)
   if i == '7':
     bottom(i)
   if i == '8':
     bottom(i)
   if i == '9':
     bottom(i)
  print()

#function to print the top line
def top(i): 

 if i == '-':
  print('   ', end='')
 if i == '0': 
  print(' _ ', end='')
 if i == '1':
  print('   ', end='')
 if i == '2':
  print(' _ ', end='')
 if i == '3':
  print(' _ ', end='')
 if i == '4':
  print('   ', end='')
 if i == '5':
  print(' _ ', end='')
 if i == '6':
  print(' _ ', end='')
 if i == '7':
  print(' _ ', end='')
 if i == '8':
  print(' _ ', end='')
 if i == '9':  
  print(' _ ', end='')

# function to print the middle line
def middle(i):

 if i == '-':
  print(' _ ', end='')
 if i == '0': 
  print('| |', end='')
 if i == '1':
  print('  |', end='')
 if i == '2':
  print(' _|', end='')
 if i == '3':
  print(' _|', end='')
 if i == '4':
  print('|_|', end='')
 if i == '5':
  print('|_ ', end='')
 if i == '6':
  print('|_ ', end='')
 if i == '7':
  print('  |', end='')
 if i == '8':
  print('|_|', end='')
 if i == '9': 
  print('|_|', end='')

# function to print the bottom line
def bottom(i):

 if i == '-':
  print('   ', end='')
 if i == '0': 
  print('|_|', end='')
 if i == '1':
  print('  |', end='')
 if i == '2':
  print('|_ ', end='')
 if i == '3':
  print(' _|', end='')
 if i == '4':
  print('  |', end='')
 if i == '5':
  print(' _|', end='')
 if i == '6':
  print('|_|', end='')
 if i == '7':
  print('  |', end='')
 if i == '8':
  print('|_|', end='')
 if i == '9':
  print(' _|', end='')
 
if __name__ == '__main__': 
  for line in sys.stdin:
    number = compute(line.split())
    fancyPrint(int(number)) 
