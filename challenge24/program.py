#!/usr/bin/env python3
import sys 
import math


def getLocation(n):
  layer = math.ceil(math.sqrt(n)) #which square you're in 
  mid = layer**2 - (layer - 1) #to get the mid point 

  if layer%2 == 0: #even 
    if n > mid:
      row = layer - abs(mid - n)
      col = layer
    else: 
      col = layer - abs(mid - n)
      row = layer

  else: # odd
    if n >= mid: 
      row = layer
      col = layer - abs(mid-n) 
    else:
      row = layer - abs(mid - n)
      col = layer 

  return row, col

if __name__== '__main__':
  for line in sys.stdin:
    x ,y = getLocation(int(line))
    print("{} = ({}, {})".format(line.strip(), x , y))
