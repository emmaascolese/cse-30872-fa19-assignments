#!/usr/bin/env python3

import sys
import collections
import time
from copy import deepcopy 

def read_graph():
    ''' Read in undirected graph '''
    g = collections.defaultdict(lambda: collections.defaultdict(int))
    so, si, connections = sys.stdin.readline().split()
    for line in range(0, int(connections)) :
      source, target, weight = sys.stdin.readline().rstrip().split()
      g[int(source)][int(target)] += int(weight)
      g[int(target)][int(source)] += int(weight)

    return int(so), int(si), g

def getPath(source, sink, g):
  frontier = []
  visited = set()
  path = dict()
  frontier.append(source)
 
  while frontier:
    node = frontier.pop(0)

    #if the node we have is the sink, return the path
    if node == sink:
      return path


    if node in visited: 
      continue

    visited.add(node)

    #go through the neighbors 
    for neighbor in g[node]:
      if neighbor not in visited and g[node][neighbor] > 0: #check if the neighbor is not in visited and that 
        path[neighbor] = node # add the destination, node to the path for retracing
        frontier.append(neighbor)

  return []

def getSolution(source, sink, g):
  total = 0
  path = getPath(source, sink, g)
  #if theres a path with the sink in it, get the flow and add that to the total until there are no more paths 
  while(sink in path):
    flow = getFlow(path, g, sink, source)
    g = deepcopy(g) #make a copy for updating purposes
    graph = updateGraph(g, flow, path, sink, source)
    total += flow #aggregate flows
    path = getPath(source, sink, graph)
  return total

def updateGraph(g, flow, path, sink, source):
  src = sink
  while src is not source:
    val = path[src]
    g[src][val] -= flow
    g[val][src] -= flow
    src = val
  return g
    

def getFlow(path, g, sink, source):
  src = sink
  flow = 10000 #number that no weight will be greater than 
  while src is not source:
    val = path[src]
    temp = g[src][val]
    if temp < flow:
      flow = temp
    src = val
  return flow
  

if __name__ == '__main__':
  count = 0 
  for line in sys.stdin:
    if line.rstrip() == "0":
      break
    source, sink ,graph = read_graph()
    total = getSolution(source, sink, graph)
    count += 1 #count for the network
    if total == None:
      total = 0
    print("Network {}: Bandwidth is {}.".format(count,total)) 
