#!/usr/bin/env python3 

import sys

def tallBlue(gold, blue):
  '''Checks if the blue team is taller than gold ''' 
  count = 0
  for i in gold:
    if i >= blue[count]:
      return False 
    count += 1
  return True

if __name__ == '__main__':
  for line in sys.stdin:
    line1 = [int(x) for x in line.split()]
    line2 = [int(x) for x in sys.stdin.readline().split()]
    print ("Yes") if tallBlue(sorted(line1), sorted(line2)) else print("No")
