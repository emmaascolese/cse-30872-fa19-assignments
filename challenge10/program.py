#!/usr/bin/env python3
import sys

def get_ones(length, ones):
  bitset = 0
  while(bitset <= (2**length) - 1):  
    bs = format(bitset,"b")
    if(bs.count("1") == ones):
      while(len(bs) < length):
        bs = '0' + bs
      print(bs)
    bitset += 1

if __name__ == '__main__':
  for index, line in enumerate(sys.stdin):
    if (index):
      print()
    length, ones = line.split()
    get_ones(int(length), int(ones))
