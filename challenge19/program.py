#!/usr/bin/env python3 

import collections
import sys 


def find_circuit(graph, start, vertex, visited, path, limit):
    ''' Recursive DFS traversal '''

    if len(visited) == limit: # if we've seen all the nodes
      if start in graph[vertex]: #if the last node has a path back to the start
        path.append(start)
        return path
      else:
        return None
    
    # Visit each unvisited outgoing edge
    for neighbor in sorted(graph[vertex]):
        if neighbor  in visited:
            continue

        # Mark visited
        visited.add(neighbor)

        # Add to path
        path.append(neighbor)
        
        # Recurse
        if find_circuit(graph, start, neighbor, visited, path, limit):
            return path

        # Remove from path, if a path isn't found
        path.pop(-1)
        
        # Unmark visited
        visited.remove(neighbor)

    # No circuit found, so return nothing (should never happen!)
    return []



def find_hamiltonian_circuit(graph, limit):
    ''' Iteratively compute subcircuit until all edges have been travsrsed or
    no circuit is possible '''
    start    = sorted(graph.keys())[0]# Starting vertex
    visited  = set()                 # Visited vertices
    visited.add(start)
    circuit  = []                    # Hamiltonian circuit (list of vertice)
    index    = 0                     # Where in circuit to insert subcircuit
    
    while start:
      circuit = find_circuit(graph,start,start,visited,[start],limit)
      
      start = None
      for neighbor in sorted(graph[start]):
        if neighbor not in visited:
          start = vertex
          break
    
    if circuit: 
      return circuit
    else:
      return None 



if __name__ == '__main__':
  for line in sys.stdin: 
      graph = collections.defaultdict(set)
      limit = int(line.rstrip())
      ln = sys.stdin.readline().rstrip().split()

      while ln[0] is not '%':
        s = int(ln[0])
        t = int(ln[1])
        graph[s].add(t)
        graph[t].add(s) 
        ln = sys.stdin.readline().rstrip().split()
      circuit = find_hamiltonian_circuit(graph,limit) 
      if not circuit:
        print("None")
      else:
        print(' '.join(str(x) for x in circuit))

