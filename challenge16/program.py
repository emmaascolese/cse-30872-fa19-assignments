#!/usr/bin/env python3

import sys 
import heapq
import collections

 
def get_graph(row, col): #creating the input 2-D array
  graph = [sys.stdin.readline().split() for x in range(row)]
  for _ in range(row):
    graph[_].insert(0,'1')
    graph[_].append('1')
  padding = ['1' for x in range(col+2)]
  graph.insert(0,padding)
  graph.append(padding)
  return graph

def getNodeList(row, col): #creating an array that's just labeled nodes 
  graph2 = []
  counter = 0
  for i in range(row):
    graph = [x+(counter*col) for x in range(col)]
    graph2.append(graph)
    counter += 1
  return graph2

def getAdjList(inp, outline, row, col): #set up the adjacency list with a default dict
  END = []
  START = '0'
  d = collections.defaultdict(dict)
  for r in range(row+1): 
    for c in range(col+1):
       if inp[r][c] != '1': 
         if inp[r][c] == 'S':
           START = outline[r-1][c-1]
         elif inp[r][c] == 'E':
           END.append(outline[r-1][c-1])
         d = getNeighbors(inp, outline, r-1, c-1, d)
  v, start, found = compute_sssp(d, START, END) 
  reconstruct_path(v, start, found)


def getNeighbors(inp, outline, r, c, result): #get the neigbors of a given node 
  #check right
  if inp[r+1][c+2] != '1':
    result[outline[r][c]][outline[r][c+1]] = 1
  #check left
  if inp[r+1][c] != '1':
    result[outline[r][c]][outline[r][c-1]] = 1
  #check up 
  if inp[r][c+1] != '1':
    result[outline[r][c]][outline[r-1][c]] = 1
  #check down
  if inp[r+2][c+1] != '1':
    result[outline[r][c]][outline[r+1][c]] = 1
  #check diagnal right up 
  if inp[r][c+2] != '1':
    result[outline[r][c]][outline[r-1][c+1]] = 2
  #check diagnal left up
  if inp[r][c] != '1':
    result[outline[r][c]][outline[r-1][c-1]] = 2
  #check diagnal down right 
  if inp[r+2][c+2] != '1':
    result[outline[r][c]][outline[r+1][c+1]] = 2
  #check diagnal down left 
  if inp[r+2][c] != '1':
    result[outline[r][c]][outline[r+1][c-1]] = 2
  return result 

def compute_sssp(g, start, END):
    ''' Use Dijkstra's Algorithm to compute the single-source shortest path '''
    frontier = []
    visited  = {}
    found = None

    heapq.heappush(frontier, (0, start, start))

    while frontier:
        weight, source, target = heapq.heappop(frontier)

        if target in visited:
            continue

        visited[target] = [source, weight]
        if target in END: #stop looking 
          found = target
          break 

        for neighbor, cost in g[target].items():
            heapq.heappush(frontier, (weight + cost, target, neighbor))

    return visited, start, found

def reconstruct_path(visited, source, target):
    ''' Reconstruct path from source to target '''
    path = []
    cost = 0

    if target != None: #check that the target has been found, fixed jey errors 
      cost = visited[target][1]
      while target != source:
        path.append(target)
        target = visited[target][0]

    if len(path) > 0:
      path.append(source)
      print('Cost: {} Path: {}'.format(cost,' '.join(map(str, reversed(path)))))
    else:
      print("Cost: 0 Path: None")  



if __name__ == '__main__':
  for line in sys.stdin: 
    row, col = line.rstrip().split()
    if int(row) == 0:
      break
    inp = get_graph(int(row), int(col))
    outline = getNodeList(int(row), int(col))
    d = getAdjList(inp, outline, int(row), int(col))
