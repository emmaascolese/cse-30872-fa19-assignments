#!/usr/bin/env python3

import sys

def getGuess(spots, flowers):
  '''uses binary search to decide guesses for optimal spacing '''
  low = 0
  high = spots[-1] - spots[0]
  
  while((high-low) > 1 ):
    middle = (high+low)//2 
    if(canFit(spots, flowers, middle)):
      low = middle
    else:
      high = middle
  
  return low

def canFit(spots, flowers, guess):
  '''checks how many flowers can be placed by comparing where we've placed one to where we can to place the next '''
  prev = spots[0]
  flower = 1 
  
  for i in spots[1:]:
    if (i - prev) > guess: 
      flower += 1
      prev = i 
    
  return flower >= flowers

if __name__ == '__main__':
  for line in sys.stdin:
    numValid, flowers = line.split()
    spots = [int(sys.stdin.readline()) for i in range(int(numValid))]
    print(getGuess(sorted(spots), int(flowers)))
