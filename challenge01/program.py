#!/usr/bin/env python

import sys

#function to make dictionary of letters and the number of occurences
def getDict(s):
  d = {}
  for x in s: 
    x = x.lower()
    d[x] = d.get(x,0) +  1
  return d 

#check how many times the second words cn be made from the first
def checkWords(d1, d2, s2):
  counter = 0
  possible = 1
  while possible:
    for x in d2.keys():
      x= x.lower()
      if x not in d1:
        possible = 0 
        break
      elif d1[x] <  d2[x]:
        possible = 0
        break
      else:
        d1[x] = d1[x] - d2[x]
    if(possible):
      counter += 1 
  return counter

if __name__ == '__main__':
  for line in sys.stdin: 
    s1, s2 = line.strip().split() 
    print(checkWords(getDict(s1), getDict(s2), s2))
