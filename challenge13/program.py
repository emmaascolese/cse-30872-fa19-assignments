#!/usr/bin/env python3
#reference https://www.geeksforgeeks.org/level-order-tree-traversal/ for ideas on how to print correctly

import sys



class Node:
  def __init__(self,info):
    self.data = info
    self.left = None
    self.right = None 
 
def formatPrint(root): 
    h = height(root) 
    for i in range(1, h+1):
        l = []  
        printLevel(root, i, l) # l gets updated 
        for j, data in enumerate(l):
          if j == 0:
            print(data, end='')
          else: 
            print(" {}".format(str(data)),end="") #only print a space before items that aren't the first
        print()
  
# Print nodes at a given level 
def printLevel(root , level, l): 
    #Base Case
    if root is None: 
        return
    #Only prints when it gets to the lowest level so no reprinting happens
    if level == 1: 
        l.append(root.data)
    #Recursive calls
    elif level > 1:
        printLevel(root.left , level-1, l) 
        printLevel(root.right , level-1, l)

  
  
def height(node): 
    if node is None: 
        return 0 
    else : 
        #compute the height of each subtree  
        lheight = height(node.left) 
        rheight = height(node.right) 
  
        #select larger subtree, account for unbalanced trees
        if lheight > rheight : 
            return lheight+1
        else: 
            return rheight+1

def bst(array):
  #Base Case
  if not array:
    return None

  #Recursive part 

  mid = (len(array)) // 2 #get the middle of the array 

  root = Node(array[mid]) #make middle of array a node 

  root.left = bst(array[:mid]) #everything to the left of the mid point 
  
  root.right = bst(array[(mid+1):]) #everything to the right of the mid point 
  
  return root
    

if __name__ == '__main__': 
  for line in sys.stdin: 
    numbers = line.rstrip().split()
    selected = bst(numbers) #get the numbers in divide and conquer order form 
    formatPrint(selected)
