#!/usr/bin/env python3
import sys 

MOVES = {
 '1' : '68',
 '2' : '79',
 '3' : '48',
 '4' : '039',
 '5' : tuple(),
 '6' : '017',
 '7' : '26',
 '8' : '13',
 '9' : '24',
 '0' : '46'
}


def moves(start, length, ans):
  
  if length == 1:
    yield ans 

  else:
    length = length - 1
    for number in MOVES.get(start):
      yield from moves(number, length , ans + number)
    

if __name__ == '__main__':
  for index, line in enumerate(sys.stdin):
    if index:
      print()
    start, length = line.split()
    for i in moves(start, int(length), start):
      print(i)
