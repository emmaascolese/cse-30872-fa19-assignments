#!/usr/bin/env python3

import collections
import sys

# Tuple for Chocolates 

Chocolate = collections.namedtuple('Chocolate', 'weight yummy')

def compute_yummy(chocolates, m, num):
  #Insert padding
  chocolates.insert(0,None) #add the no turtle to the front 
  
  #Initialize yummy table:
  
  yummy = [[0 for _ in range(m+1)] for _ in range(len(chocolates))]
  
  #If we're in the 0 column, then the yumminess is 0 
  for i in range(len(chocolates)):
    yummy[i][0] = 0

  for row in range(1, num+1): 
    for weights in range(1, m+1):
      print("looking at: ", chocolates[row].weight, " ", chocolates[row].yummy)
      print("max weight: ", weights)
      if chocolates[row].weight == weights: 
        yummy[row][weights] = yummy[row-1][weights] + chocolates[row].yummy
      elif chocolates[row].weight < weights:
        yummy[row][weights] = yummy[row-1][weights] + chocolates[row].yummy
        new = m - row
        if new + chocolates[row].weight <= weights: #weight of other bar + weight of bar were lookign at 
          yummy[row][weights] = yummy[row][weights-1] +  yummy[row][new]
      else:
        yummy[row][weights] = yummy[row-1][weights]
      print(yummy[row][weights])

  return yummy



if __name__=='__main__':
  Chocolates = [Chocolate(3, 50), Chocolate(4,40), Chocolate(5,10), Chocolate(6,30)]
  for i in compute_yummy(Chocolates, 10, 4):
    print(i)
