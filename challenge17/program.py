#!/usr/bin/env python3 
import sys
import collections
import math
import heapq

def get_points(limit):
  points = []
  for i in range(limit):
    x,y = sys.stdin.readline().split()
    points.append((float(x),float(y)))
  return points

def getAdjacencyList(points):
  g = collections.defaultdict(dict)
  for i in range(len(points)):
    for j in range(i+1,len(points)):
      
      g = distance(points[i],points[j], g)
      '''g[i][j] = dist  
      g[j][i] = dist'''
  return g

def compute_mst(g):
    totalCost = 0
    frontier = []
    visited  = {}
    start    = list(g.keys())[0]

    heapq.heappush(frontier, (0, start, start))

    while frontier:
        weight, source, target = heapq.heappop(frontier)

        if target in visited:
            continue

        totalCost += weight
        visited[target] = source

        for neighbor, cost in g[target].items():
            heapq.heappush(frontier, (cost, target, neighbor))

    return totalCost

'''def reconstruct_path(visited, source, target):
    path = []
    curr = target

    while curr != source:
        path.append(curr)
        curr = visited[curr]

    path.append(source)
    return reversed(path)'''


def distance(x, y, g):
  d = math.sqrt( (x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)
  g[x][y] = d
  g[y][x] = d
  return g
if __name__ == '__main__':
  for line in sys.stdin:
    limit = line
    if int(limit) == 0:
      break 
    points = get_points(int(limit))
    g = getAdjacencyList(points)
    print("{:.2f}".format((compute_mst(g))))
    
    
