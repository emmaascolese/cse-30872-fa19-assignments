#!/usr/bin/env python3 
import sys
import math

def maxFour(nums):
  total = nums[0] + nums[1]*2 + nums[2]*3 + nums[3]*4
  newTotal = math.ceil(total/4) 
  return newTotal

if __name__ == '__main__':
  for line in sys.stdin:
    numbers = [x for x in map(int, line.split())]
    print(maxFour(numbers)) 
