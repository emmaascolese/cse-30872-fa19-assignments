#!/usr/bin/env python3

import sys 

def read_graph(n, m):
    ''' Construct adjacency set '''
    g = {v: set() for v in range(n)}

    for _ in range(m):
        s, t = map(int, sys.stdin.readline().split())
        g[s].add(t)
        g[t].add(s)

    return g

def getPath(graph, node, point, marked, count):
  temp = 0  
  tup1 = (node, point) 
  tup2 = (point, node)

  #base cases
  if tup1 in marked: 
    return count - 1  
  if tup2 in marked:
    return count - 1

  marked.add(tup1) #adding both tuples to marked 
  marked.add(tup2)

  # recursive calls 
  for i in graph[node]:
    temp = max(getPath(graph, i, node, marked, count+1) , temp)
  
  return temp

if __name__ == '__main__':
  n, m = map(int, sys.stdin.readline().split())
  while n and m: 
    results = []
    graph = read_graph(n,m)
    for i in graph:
      marked = set()
      results.append(getPath(graph, i , 1000 , marked, 0)) #1000 will never be a real target node, using it as a starting point
    if results:
      print(max(results))
    n, m = map(int, sys.stdin.readline().split()) 
