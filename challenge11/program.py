#!/usr/bin/env python3
import sys
import math

inf = float("inf")

def read_grid(n):
  # Pad grid
  grid = [[inf for _ in range(n + 1)]]
  for _ in range(n):
      grid.append([inf] + list(map(int, sys.stdin.readline().split())))

  return grid

def moana(grid, n):
  table = [[0 for _ in range(n+1)] for _ in range(n+1)]
  for i in range(n+1):
    table[0][i] = inf
    table[i][0] = inf

  #get the first row
  for row in range(1, n+1):
    for col in range(1, n+1):
      x =  min(grid[row][col-1], grid[row-1][col], grid[row-1][col-1]) 
      if x == inf:
        table[row][col] = grid[row][col]
        continue
      table[row][col] = grid[row][col] + min(table[row][col-1], 
                        table[row-1][col], table[row-1][col-1])
  return table

def backtrack(grid, table, n):
  result = []
  row  = n 
  col  = n 
  result.insert(0, grid[row][col])
  while col > 0 or row > 0:
    mini = inf
    if table[row][col-1] < mini: #check horizontal
      mini = table[row][col-1] 
      r1 = row     #potential next row
      c1 = col - 1 #potential next col
    if table[row-1][col] < mini: #check vertical
      mini = table[row-1][col]
      r1 = row - 1 
      c1 = col 
    if table[row-1][col-1] < mini: #check diagnol 
      mini = table[row-1][col-1]
      r1 = row - 1 
      c1 = col - 1
    if mini == inf:
      break
    row = r1 
    col = c1 
    result.insert(0,grid[row][col])  
  return result  
 
if __name__ == '__main__':
 while True:
   try:
     n = int(sys.stdin.readline()) #get the range
     if n == 0: 
       break
   except ValueError:
     break  
   grid = read_grid(n)
   table = moana(grid,n) 
   path = backtrack(grid,table,n)
   print(table[n][n])
   print(' '.join(map(str,path)))
