#include <iostream>
using namespace std;
#include <string>
#include <vector>

int getTidy(int);

int main(){

  int n, test, tidy; 
  vector<int> result; 
 
  cin >> n; //number of test cases there'll be 

  for(int i=0; i<n; i++){ //perdeck
    cin >> test; 
  
    tidy = getTidy(test);
    result.push_back(tidy); 
  
  }
  //print results
  for(int p=1; p<=result.size(); p++){
    cout << "Deck #" << p << ": " << result[p-1] << endl; 
  }
return 0;
}

//find the largest tidy number
int getTidy(int test){
  bool untidy = false; 
  string ptidy="1", td="1"; 
  for(int j=test; j>=1; j--){ //per number in the deck
    ptidy = to_string(j);
    if(ptidy.length() == 1){
      return stoi(ptidy);
      break; 
    }
   
    auto p1 = ptidy.begin(); 
    auto p2 = ptidy.begin()+1;

    while( *p2 != 0){ //checking within the number if it's tidy
      untidy=false; 
      if(*p1>*p2){
        untidy = true;
        break; //if it's untidy, stop looking 
      }
      else{
        p1++; 
        p2++;
     }
    }  
    if (!untidy){
      return stoi(ptidy);
      break; 
    }
  }
}
