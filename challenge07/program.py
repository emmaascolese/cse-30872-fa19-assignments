#!/usr/bin/env python3

import sys 
import itertools 

'''dic = {
 '0' : False,
 '1' : False,
 '2' : False,
 '3' : False,
 '4' : False,
 '5' : False,
 '6' : False,
 '7' : False,
 '8' : False,
 '9' : False
}'''

def getAnswers(x):
  check = False
   
  for d in range(1234, 56790):
    n = x * d
    
    if n > 98765:
      break
    elif n < 10000:
      n = '0' + str(n)
    else: 
      n = str(n)
    
    if d < 10000:
      d = '0' + str(d)
    else: 
      d = str(d)

    if(proper(n,d)):
      print('{} / {} = {}'.format(n,d,x))
      check = True 
  if not check: 
    print('There are no solutions for {}.'.format(x)) 

def proper(n,d):  
  dic = {} 
  for i in range(0,10):
    dic[i] = False

  for i in dic.keys():
    if str(i) in n: 
      dic[i] = True
    if str(i) in d: 
      dic[i] = True 
  
  if False in dic.values(): 
      return False 
  else: 
      return True


if __name__ == '__main__':
  for index, line in enumerate(sys.stdin):
    if (index  and int(line)): 
      print()
    if(int(line)): 
      getAnswers(int(line))

