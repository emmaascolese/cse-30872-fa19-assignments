#!/usr/bin/env python3 

import sys

OPERATIONS = ('-', '+', '*', '/')
PAREN = ('(', ')')
DIGIT = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

class Node:
  def __init__(self, value, children):
    self.value = value
    self.children = children

def parse_token(s): 
  token = ""
  if len(s) > 0:
    char = s.pop(0) #get the first character
    while char == " ":
      char = s.pop(0)
    if char in OPERATIONS or PAREN:
      token = char 
    elif char in DIGIT:
      while char in DIGIT:
        token += char 
        if len(s) <= 1: #see if there's anything in the list
          break
        char = s.pop(0)
        if char == ')': #if it's ) put it back so parse_exp hits base case
          s.insert(0,char)
  return token 

def parse_exp(line):
   children = []
   token = parse_token(line)

   if token == ')':
     return children

   if token == '(':
     token = parse_token(line) #get the next item in the string
     child = parse_exp(line)
     while child:
       children.append(child)
       child = parse_exp(line)    

   return Node(token, children)

def getSolution(tree, stack):
  if tree.value not in OPERATIONS and tree.value not in PAREN:
    stack.append(int(tree.value))
    return stack

  for i in tree.children: #add numbers to the stack
    stack = getSolution(i, stack)
    
  if tree.value in OPERATIONS:
    nums = []

    for i in range(len(tree.children)):
      nums.append(stack.pop())

    for i in range(len(nums)-1):
      num1 = nums.pop()
      num2 = nums.pop()
      if tree.value == "+":
        nums.append(int(num1) + int(num2))
      if tree.value == "-":
        nums.append(int(num1) - int(num2))
      if tree.value == "*":
        nums.append(int(num1) * int(num2)) 
      if tree.value == "/":
        nums.append(int(num1) / int(num2))
    stack.extend(nums) #put the numbers on the stack 
  return stack

if __name__ == '__main__':
  for line in sys.stdin: 
    tree = parse_exp(list(line.strip()))    
    stack = []
    solution = getSolution(tree, stack)
    print(int(sum(solution))) 
