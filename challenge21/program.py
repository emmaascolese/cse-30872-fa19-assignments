#!/usr/bin/env python3 

import sys 
import math 

def compute_primes(n):
  primes = set(range(2, n+1))
  for i in range(2,n+1):
    for m in range(2*i, n+1, i):
      try:
        primes.remove(m)
      except KeyError:
        pass 
  return primes 

def getCar(n):
  var = False 
  for i in range(n): 
   if pow(i, n, n) == i:
     var = True 
   else:
     return False 
  return var

if __name__ == '__main__':
  for line in sys.stdin:
    number = int(line.strip())
    primes = compute_primes(number)
    if number == 1:
      print(number, "is normal.")
    elif number in primes:
      print(number, "is normal.")
    elif getCar(number):
      print("The number", number, "is a Carmichael number.")
    else:
      print(number, "is normal.")
    

