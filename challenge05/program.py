#!/usr/bin/env python3 
 
import sys 

Cycles = {}

def cycles(n):
  if n not in Cycles: 
    if n % 2 == 0:
      Cycles[n] = cycles(n//2)[0] + 1 #evens 
    elif n == 1: 
      Cycles[n] = 1 #base case 
    else: 
      Cycles[n] = cycles(3*n + 1)[0] +1 #odds 

  return Cycles[n],n   
  
if __name__ == '__main__':
  for line in sys.stdin:
    n1, n2 = line.split()
    pairs = []
    for x in range(int(n1),int(n2)):
       pairs.append(cycles(x))
    if(len(pairs) > 0): 
       pairMax = max(pairs, key=lambda item:item[0])
    print(n1, n2, pairMax[1] , pairMax[0]) 
