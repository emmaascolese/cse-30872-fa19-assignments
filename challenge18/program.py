#!/usr/bin/env python3

import collections
import sys

# Graph Structure

Graph = collections.namedtuple('Graph', 'edges degrees')

# Read Graph

def read_graph():
    edges   = collections.defaultdict(set)
    degrees = collections.defaultdict(int)

    for line in sys.stdin:
        
        sources  = [x for x in line.strip()] #split up the line

        for source in range(len(sources)-1): #loop until second to last 
           if sources[source+1] not in edges[sources[source]]: 
              degrees[sources[source+1]] += 1 #increase degree if it's the first time seeing this source, target pair 
           edges[sources[source]].add(sources[source+1]) # add neighbor to edges
           degrees[sources[source]] # make sure a vertices

    return Graph(edges, degrees)

# Topological Sort

def topological_sort(graph):
    frontier = [v for v, d in graph.degrees.items() if d == 0]
    visited  = []

    while frontier:
        vertex = frontier.pop()
        visited.append(vertex)

        for neighbor in graph.edges[vertex]:
            graph.degrees[neighbor] -= 1
            if graph.degrees[neighbor] == 0:
                frontier.append(neighbor)

    return visited

# Main Execution

if __name__ == '__main__':
    graph    = read_graph()
    vertices = topological_sort(graph)
    result = ""
    for x in vertices:
      result += x
    print(result)

