#!/usr/bin/env python3

import sys

nums = {} #used for memoizing  

def getTotal(n):

  if n not in nums:
    #Base Cases
    if n == 1:
      return 2 #2 because 1 and 4 are the same 

    if n == 0:
      return 1

    if n < 0:
      return 0

    #Recursive call
    nums[n] = 2*(getTotal(n-1)) + getTotal(n-2) + getTotal(n-3)  

  return nums[n] 

if __name__ == '__main__': 
  for line in sys.stdin:
    total = getTotal(int(line))
    print(total)
