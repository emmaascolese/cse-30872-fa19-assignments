#!/usr/bin/env python3 

import sys 

def getPaths(root, binary, path, current, tmp):
  
  if int(root) >= len(path):
    return

  current = current + path[int(root)]  #add the node we're looking at to the result
  
  tmp = tmp + str(root)

  if len(current) > len(binary):
    current = current[1:]
  
  if binary == current:
    s.add(tmp)

  #check left 
  getPaths((2*int(root)+1), binary, path, current, tmp)
  
  #check right 
  getPaths((2*int(root)+2), binary, path, current, tmp)


if __name__ == '__main__':
  for line in sys.stdin:
    number, tree = line.rstrip().split()
    binary = bin(int(number))
    binary = binary[2:]
    s = set() 
    getPaths(0,str(binary),tree,"", "")
    print("Paths that form {} in binary ({}): {}". format(number, binary,len(s)))
