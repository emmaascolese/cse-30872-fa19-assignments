#!/usr/bin/env python3

import sys 

def getLongest(fruits):
  longest=[]
  index = 0
  left = 0
  d = {}
  while( index < len(fruits)):
    if fruits[index] not in d:
      d[fruits[index]] = index 
      index += 1
    else:
      if(d[fruits[index]] >= left and len(longest) < (index-left)):
        longest=fruits[left:index]
      if(d[fruits[index]] >= left):
        left = d[fruits[index]]+1 
      d[fruits[index]] = index
      index += 1
     

  if(len(longest) < len(fruits[left:])):
    longest = fruits[left:]
  return longest

if __name__ == '__main__':
  for line in sys.stdin:
    fruits = getLongest(line.split())
    print('%d: %s' % (len(fruits),fruits[0]),end='')
    for item in fruits[1:]: 
      print(",",item,end='')
    print() 
